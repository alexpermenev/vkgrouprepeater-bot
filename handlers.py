from telegram.ext import *
from telegram import Bot

from message import Message

help_text = '''
Все команды бота:

/help - Вывести все команды :D
/subscribe - Подписаться на рассылку бота
/unsubscribe - Отписаться от рассылки
'''

subscribers = set()
try:
    fin = open("subs.txt", "r")
    subscribers = set(map(int, fin.readlines()))
    fin.close()
except:
    pass


def start_handler(update, context):
    update.message.reply_text('Привет! Этот бот предназначен для рассылки подписчикам новых постов из группы: '
                              'https://vk.com/public196955384\n\n' + help_text)


def help_handler(update, context):
    update.message.reply_text(help_text)


def subscribe_handler(update, context):
    subscribers.add(update.message.from_user.id)
    fout = open("subs.txt", "w")
    fout.writelines(map(str, subscribers))
    fout.close()
    update.message.reply_text("Ты успешно подписался!")


def unsubscribe_handler(update, context):
    subscribers.remove(update.message.from_user.id)
    fout = open("subs.txt", "w")
    fout.writelines(map(str, subscribers))
    fout.close()
    update.message.reply_text("Как жаль что ты отписался :(")


def any_message(update, context):
    update.message.reply_text("Команда не поддерживается\n\n" + help_text)


def register_handlers(dispatcher):
    dispatcher.add_handler(CommandHandler("start", start_handler))
    dispatcher.add_handler(CommandHandler("help", help_handler))
    dispatcher.add_handler(CommandHandler("subscribe", subscribe_handler))
    dispatcher.add_handler(CommandHandler("unsubscribe", unsubscribe_handler))
    dispatcher.add_handler(MessageHandler(Filters.text, any_message))


def mail_all(bot: Bot, message: Message):
    for user_id in subscribers:
        message.send(bot, user_id)
